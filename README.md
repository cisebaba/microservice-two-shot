# Wardrobify

DreamTeam:

* Amanda Kiehm - Hats
* Cise Babatasi - Shoes

## Design

## Shoes microservice

Shoe and BinVo models are created in the shoe microservice.
Bin polling function is getting instance from wardrope monolith and passing into out BinVO.
Shoe list,delete and create restful api's and path are created. We are creating states on the front-end and connecting with url so services can do inside communication. Since we created that request&respose relation,delete button activated and made it functional  Delete button added, product details showed on the list as well. 


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Approach: Refer to Conference GO!
Start with creating models for Hat and LocationVO and then implement small pieces of functionality at a time. First, implement Hat list api view. Create a React component to display the hat list. Next, work on the create view and react form. Then, implement functionality to delete a hat. Last, make everything look prettier.

