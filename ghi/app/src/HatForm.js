import React from 'react';


class HatForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            fabric: '',
            styleName: '',
            color: '',
            locations: []
        };
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.style_name = data.styleName;
        delete data.styleName;
        delete data.locations;
        

        const hatUrl = 'http://localhost:8090/api/hats_rest/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);

        if (response.ok) {
            const cleared = {
                fabric: '',
                styleName: '',
                color: '',
                location: '',
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
        }
    }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({fabric: value});
    }

    handleStyleNameChange(event) {
        const value = event.target.value;
        this.setState({styleName: value});
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value});
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value});
    }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                <form onSubmit={this.handleSubmit} id="create-hat-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleFabricChange} name="fabric" placeholder="Fabric" required
                        type="text" id="fabric" value={this.state.fabric}
                        className="form-control" />
                    <label htmlFor="name">Fabric</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleStyleNameChange} name="style_name" placeholder="Style Name" required
                        type="text" id="style_name" value={this.state.styleName}
                        className="form-control" />
                    <label htmlFor="starts">Style Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleColorChange} name="color" placeholder="Color" required 
                        type="color" id="color" value={this.state.color}
                        className="form-control" />
                    <label htmlFor="room_count">Color</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleLocationChange} name="location" required id="location" 
                        value={this.state.location} className="form-select">
                    <option value="">Choose a location</option>
                    {this.state.locations.map(location => {
                        return (
                            <option key={location.href} value={location.href}>
                            {location.closet_name} - {location.section_number}/{location.shelf_number}
                            </option>
                        );
                    })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
    }
}

export default HatForm;