import {Link} from 'react-router-dom';

// function MainPage() {
//   return (
//     <div className="px-4 py-5 my-5 text-center">
//       <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
//       <div className="col-lg-6 mx-auto">
//         <p className="lead mb-4">
//           Need to keep track of your shoes and hats? We have
//           the solution for you!
//         </p>
//         <div className="d-grid gap-2 col-6 mx-auto">
//               <Link to="shoes/new" className="btn btn-outline-success"> Add a shoe</Link>
//               <Link to="hats/new" className="btn btn-outline-success"> Add a hat</Link>
//         </div>
//       </div>
//     </div>
//   );
// }

// export default MainPage;


function MainPage() {
  return (
    <div className="container">


        <div className="px-4 py-5 my-5 text-center">
          <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              Need to keep track of your shoes and hats? We have
              the solution for you!
            </p>
            <div className="d-grid gap-2 col-6 mx-auto">
                  <Link to="shoes/new" className="btn btn-outline-success"> Add a shoe</Link>
                  <Link to="hats/new" className="btn btn-outline-success"> Add a hat</Link>
                  <div className="b-example-divider"></div>

            </div>
            <div className="container">
                <img src="https://images.pexels.com/photos/5704845/pexels-photo-5704845.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" 
                  className="img-fluid border rounded-3 shadow-lg mb-4" />
              
            </div>


          </div>
        </div>

    </div>
  );
}

export default MainPage;