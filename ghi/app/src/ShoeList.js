import React from "react";

class ShoeList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            shoes:[]
        }
    }

    async componentDidMount(){
        const url = 'http://localhost:8080/api/shoes_rest';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            this.setState({shoes: data.shoes})
        }
    }

    async handleDelete(id, i){
        // console.log(id, i)
        const deleteURl = `http://localhost:8080/api/shoes_rest/${id}`
        const response = await fetch(deleteURl, {method: "delete"});
        if (response.ok){
            this.state.shoes.splice(i,1)
            this.setState({shoes: this.state.shoes})
        }
    }
    
    render(){
        return(
            <div className="col">
                <h1>Shoes</h1>
                {this.state.shoes.map((shoe,i) => {
                    // const shoe = data.shoe;
                    console.log(shoe)
                    return (
                    <div key={shoe.id} className="card mb-3 mt-3 shadow"style={{maxWidth:480}}>
                        <img src={shoe.image} className="card-img-top" />
                            <div className="card-body">
                            <h5 className="card-title">Manufacturer: {shoe.manufacturer}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">Model Name: {shoe.name}</h6>
                            <p className="card-text">Color:{shoe.color}</p>
                            <h6 className="card-subtitle mb-2 text-muted">Bin: {shoe.bin}</h6>
                            <button className="btn btn-danger" style={{float:"right"}} onClick={()=>this.handleDelete(shoe.id, i)}> Delete </button>
                        </div>
                    </div>
                    );
                })}
            </div>

            
        )
    }
}
export default ShoeList;