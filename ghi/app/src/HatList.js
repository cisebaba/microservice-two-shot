import React from "react";
import { Link } from "react-router-dom";


class HatList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            hats:[]
        };
        this.handleRemove = this.handleRemove.bind(this);
    }

    async componentDidMount(){
        const url = 'http://localhost:8090/api/hats_rest/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            this.setState({hats: data.hats})
        }
    }

    async handleRemove(id) {
        const hatUrl = 'http://localhost:8090/api/hats_rest/' + id + '/';
        const fetchConfig = {
            method: "delete"};
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const hatsToKeep = this.state.hats.filter(hat => (
              hat.id !== id
            ))
            this.setState({hats: hatsToKeep});
        }
    }

    
    render(){
        return(
          <div>
          <h1>My Hats</h1>
            <div className="col-md">
                {this.state.hats.map(hat => {
                    return (
                      <div key={hat.id} className="card text-grey mb-3 w-50 shadow">
                        <div className="card-header bg-info">
                          Hat Location: {hat.location.closet_name}
                        </div>
                          <img src={hat.picture_url} className="card-img-top" />
                              <div className="card-body">
                              <h5 className="card-title">{hat.fabric}</h5>
                              <h6 className="card-subtitle mb-2 text-muted">{hat.style_name}</h6>
                              <p className="card-text">{hat.color}</p>
                              <button className="btn btn-primary" onClick={() => this.handleRemove(hat.id)} to="">Delete</button>
                          </div>
                      </div>
                    );
                })}
            </div>
          </div>
        )
    }
}
export default HatList;