import React from "react";

class ShoeForm extends React.Component {
    constructor(props){
        super(props);
        this.state={
            manufacturer: '',
            name: '',
            color: '',
            image: '',
            bin: '',
            bins: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangeImage = this.handleChangeImage.bind(this);
        this.handleChangeBin = this.handleChangeBin.bind(this);
    }
    async componentDidMount(){
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if(response.ok) {
            const data = await response.json();
            this.setState({bins:data.bins});
        }
    }
    async handleSubmit(event){
        event.preventDefault();
        const data ={...this.state};
        delete data.bins;

        const shoeUrl = 'http://localhost:8080/api/shoes_rest/';
        const fetchConfig ={
            method:"post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl,fetchConfig);
        if(response.ok){
            const newShoe = await response.json();
            console.log(newShoe);
            this.setState({
                manufacturer: '',
                name: '',
                color: '',
                image: '',
                bin: '',
            });
        }
    }
    handleChangeManufacturer(event){
        const value = event.target.value;
        this.setState({manufacturer:value});
    }
    handleChangeName(event){
        const value = event.target.value;
        this.setState({name:value});
    }
    handleChangeColor(event){
        const value = event.target.value;
        this.setState({color:value});
    }
    handleChangeImage(event){
        const value = event.target.value;
        this.setState({image:value});
    }
    handleChangeBin(event){
        const value = event.target.value;
        this.setState({bin:value});
    }
    render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new Shoe</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeManufacturer} placeholder="Manufacturer" required type="text" name="Manufacturer" id="Manufacturer" value={this.state.manufacturer} className="form-control" />
                    <label htmlFor="manufacturer">Manufacturer</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeName} placeholder="Name" required type="text" name="Name" id="Name" value={this.state.name} className="form-control" />
                    <label htmlFor="name">Model Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeColor} placeholder="Ends" required type="text" name="color" id="color" value={this.state.color} className="form-control" />
                    <label htmlFor="color">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeImage} placeholder="Image" type="text" name="Image" id="Image" value={this.state.image} className="form-control" />
                    <label htmlFor="Image">Image</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleChangeBin} required name="bin" id="bin" value={this.state.bin} className="form-select">
                      <option value="">Choose a Bin</option>
                      {this.state.bins.map(bin => {
                          console.log(bin)
                        return (
                          <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                        )
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
}
export default ShoeForm;