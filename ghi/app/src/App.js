import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import ShoeList from './ShoeList'; 
import HatForm from './HatForm';
import ShoeForm from './ShoeForm';


function App(props) {
  // if (props.hats === undefined) {
  //   return null;
  // }
  // if (props.shoes === undefined){
  //   return null;
  // }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoeList shoes={props.shoes}/>}/>
          <Route path='shoes/new' element={<ShoeForm/>}/>
          <Route path="hats">
              <Route path="" element={<HatList hats={props.hats} />} />
              <Route path="new" element={<HatForm />} />
            </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
